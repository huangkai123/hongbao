#coding:utf-8
import requests
import time
import random
import subprocess
import json
import sys
import os
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import shutil
import subprocess

# 记录
# http://f.valuecome.com//choujiang/open   #记录打开链接的次数
# http://f.valuecome.com//choujiang/prize  #用于记录点击抽奖的次数

def get_uas():
    print('获取UA')
    while True:
        try:
            resp = requests.get('http://f.valuecome.com/wings/ua', timeout=20)
        except (requests.exceptions.Timeout, requests.exceptions.ConnectionError):
            print('connection timeout getting ua list')
            time.sleep(5)
            continue
        else:
            if resp.status_code >= 400:
                print('status_code=%s on getting task list' % resp.status_code)
                time.sleep(5)
                continue
            try:
                ua_lists = resp.json()
            except ValueError as e:
                print('error from ua %s' % (resp.text))
                time.sleep(5)
                continue
            else:
                break
    return ua_lists

def kill_chrome():
    try:
        subprocess.Popen(['TASKKILL', '/IM', 'chrome.exe', '/F']).wait()
    except BaseException as e:
        pass

    try:
        subprocess.Popen(['TASKKILL', '/IM', 'chromedriver.exe', '/F']).wait()
    except BaseException as e:
        pass
    
    try:
        subprocess.Popen(['killall', 'chrome']).wait()
    except BaseException as e:
        pass

    try:
        subprocess.Popen(['killall', 'chromedriver']).wait()
    except BaseException as e:
        pass

    try:
        subprocess.Popen(['TASKKILL', '/IM', 'WerFault.exe', '/F']).wait()
    except BaseException as _:
        pass


# 开始
if __name__ == '__main__':
    # 1. 获取联网所需要的信息
    print('获取联网信息')
    with open('config.json', 'r', encoding='utf-8') as f:
        fconfig = json.load(f)
    net_wait = fconfig.get('net_wait', 5)
    net_name = fconfig['net_name']
    net_pwd = fconfig['net_pwd']
    this_net_wait = net_wait

    # 2. 读取IP存档
    if os.path.isfile('ippool.json'):
        print('有IP记录')
        with open('ippool.json', 'r', encoding='utf-8') as f:
            ippool = json.load(f)
    else:
        ippool = []


    open_count = 0
    click_count = 0

    while True:
        print('清除chrome进程')
        kill_chrome()
        # 设置运行次数
        if open_count == 500:
            print('500次运行完成')
            break

        connected = False
        while not connected:
                # print('ippool', ippool)
                print('链接宽带')
                try:
                    subprocess.Popen(["rasphone", "-h", "宽带连接"]).communicate()
                except BaseException as e:
                    sys.stderr.write('%s\n' % e)

                print('sleep %s' % this_net_wait)
                time.sleep(this_net_wait)

                try:
                    subprocess.Popen(["rasdial", "宽带连接", net_name, net_pwd]).communicate()
                except BaseException as e:
                    sys.stderr.write('%s\n' % e)
                
                try:
                    print('connet baidu confirm')
                    resp = requests.get('https://www.baidu.com/', timeout=10)
                except requests.exceptions.Timeout as e:
                    sys.stderr.write(str(e))
                    sys.stderr.write('\n')
                    print('sleep 10')
                    time.sleep(10)
                    continue
                else:
                    print('get ip')
                    try:
                        resp = requests.get('http://f.valuecome.com/ip', timeout=10)
                    except requests.exceptions.Timeout as e:
                        sys.stderr.write(str(e))
                        sys.stderr.write('\n')
                        print('sleep 10')
                        time.sleep(10)
                        continue
                    else:
                        if resp.status_code < 400:
                            #######################正常运行时候删除ippool = []####################################
                            ippool = [] #调试时候使用,不删除不能进行IP去重
                            print('connected')
                            ip = resp.text.strip()
                            if ip in ippool:
                                print('IP: %s 已经被使用，重新获取' % ip)
                                continue
                            else:
                                ippool.append(ip)
                                connected = True
                                break
                print('宽带链接成功')

        print('配置chrome参数')
        url = 'http://netkr.sunrtb.cn/c?m=netkr&v=1.909.178&fc=1&l=http://m.bianxianmao.com?appKey=d44f5aef3c17445385c5e686d2a9e8af&appType=app&appEntrance=3&business=money&i=__IMEI__&f=__IDFA__'
        options = Options()
        curdir = os.getcwd()

        userdir = '%s/%s' % (curdir, 'choujiang')
        try:
            shutil.rmtree(userdir)
        except BaseException as e:
            pass
        try:
            os.makedirs(userdir)
        except BaseException as e:
            pass
        print('userdir', userdir)

        ua = random.choice(get_uas())
        options.add_argument('--user-data-dir=%s' % userdir)
        options.add_argument("--disable-notifications")
        options.add_argument("--incognito")
        # {'profile.default_content_settings.popups': 0, 'download.default_directory': '%s' % curdir}
        prefs = {'profile.default_content_settings.popups': 0}
        options.add_experimental_option('prefs', prefs)
        options.add_argument('user-agent=%s' % ua)
        print('ua', ua)

        #  r'C:\chrome\chromedriver.exe'
        #  r'/usr/bin/chromedriver
		#C:\Program Files (x86)\Google\Chrome\Application
		#/usr/bin/chromedriver
        driver = webdriver.Chrome(executable_path = r'C:\Program Files (x86)\Google\Chrome\Application\chromedriver.exe',chrome_options=options)
        print('打开链接')
        driver.get(url)

        time.sleep(2)

        click_ratio = 1
        temp = random.random()
        if temp <= click_ratio:
            click_status = True
        else:
            click_status = False

        click_num = 3
        if click_status:
            while click_num:
                click_count = 4 - click_num
                print('第 %s 次点击抽奖' % click_count)
                start_xpath = '/html/body/div/div/div[2]/div[3]/div[2]/div[2]'
                try:
                    print('点击抽奖')
                    start = WebDriverWait(driver, 10).until(
                        EC.presence_of_element_located((By.XPATH, start_xpath))
                    )
                except BaseException as e:
                    print('点击开始抽奖出错',e)
                
                # time.sleep(2)
                # # 保存开始页面
                # with open('startresp.html', 'w', encoding='utf-8') as f:
                #     f.write(driver.page_source)
                # start.click()

                print('点击弹窗领取东西')
                prize_xpath = '/html/body/div[2]/div[3]/div[2]/div[2]'
                try:
                    prize = WebDriverWait(driver, 10).until(
                        # EC.presence_of_element_located((By.XPATH, prize_xpath))
                        EC.visibility_of_element_located((By.XPATH, prize_xpath))
                    )
                except BaseException as e:
                    print('点击抽奖选项失败', e)
                    print('保存网页源码')
                    time.sleep(3)
                    source_code = driver.page_source
                    timestamp = datetime.datetime.now().strftime('%Y-%m-%d %H-%m-%S')
                    with open('fail_%s.html' % timestamp, 'w', encoding='utf-8') as f:
                        f.write(source_code)
                    print('退出')
                    driver.quit()
                else:
                    # 抽奖页面
                    # time.sleep(3)
                    # with open('prizetresp.html', 'w', encoding='utf-8') as f:
                    #     f.write(driver.page_source)
                    prize.click()
                    print('完成抽奖,返回首页再次抽奖')
                    click_num -= 1

                time.sleep(5) #不停顿,页面不会跳转
                if click_count:
                    print('打开抽奖页面')
                    print(url)
                    driver.get(url)
                
            print('统计抽奖次数')
            resp = requests.get('http://f.valuecome.com/choujiang/prize', timeout=20)
        else:
            print('本次不抽奖')
            time.sleep(3)

        print('统计打开链接次数')
        resp = requests.get('http://f.valuecome.com/choujiang/open', timeout=20)
        
        driver.quit()
        
        print('清除chrome进程')
        kill_chrome()

        print('IP存档')
        with open('ippool.json', 'w', encoding='utf-8') as f:
            f.write(json.dumps(ippool))
        print('断开宽带')
        try:
            subprocess.Popen(["rasdial", "宽带连接", '/disconnect']).communicate()
        except BaseException as e:
            print(e)